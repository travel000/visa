$(function () {
    $('.scroll').on('click', function (event) {

        var href = this.getAttribute('href')
        var target = $(href)
        if (target.length) {
            event.preventDefault()
            $('html, body').stop().animate({
                scrollTop: (target.offset().top)
            }, 1000)
        }
    })

	var scrollPos = 0
	$(window).scroll(function(){
	   var st = $(this).scrollTop()
	   if (st > scrollPos){
	     $('.scroll').fadeOut()
	   } else {
	     $('.scroll').fadeIn()
	   }
	   scrollPos = st
	})

})